# VDR plugin language source file.
# Copyright (C) 2007 Klaus Schmidinger <kls@cadsoft.de>
# This file is distributed under the same license as the VDR package.
#
msgid ""
msgstr ""
"Project-Id-Version: Xineliboutput 1.1.0\n"
"Report-Msgid-Bugs-To: <phintuka@users.sourceforge.net>\n"
"POT-Creation-Date: 2020-01-22 11:52+0200\n"
"PO-Revision-Date: 2011-07-10 02:27+0100\n"
"Last-Translator: Diego Pierotto <vdr-italian@tiscali.it>\n"
"Language-Team: Italian <vdr@linuxtv.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Italian\n"
"X-Poedit-Country: ITALY\n"
"X-Poedit-SourceCharset: utf-8\n"

msgid "custom"
msgstr "personalizza"

msgid "tiny"
msgstr "molto piccolo"

msgid "small"
msgstr "piccolo"

msgid "medium"
msgstr "medio"

msgid "large"
msgstr "grande"

msgid "huge"
msgstr "enorme"

msgid "automatic"
msgstr "automatica"

msgid "default"
msgstr "predefinita"

msgid "Pan&Scan"
msgstr "Pan&Scan"

msgid "CenterCutOut"
msgstr "CenterCutOut"

msgid "square"
msgstr "quadrato"

msgid "anamorphic"
msgstr "anamorfico"

msgid "DVB"
msgstr "DVB"

msgid "off"
msgstr "spento"

msgid "no audio"
msgstr "niente audio"

msgid "no video"
msgstr "niente video"

msgid "Off"
msgstr "Spento"

msgid "Goom"
msgstr "Goom"

msgid "Oscilloscope"
msgstr "Oscilloscopio"

msgid "FFT Scope"
msgstr "Spettro FFT"

msgid "FFT Graph"
msgstr "Grafico FFT"

msgid "Image"
msgstr ""

msgid "Mono 1.0"
msgstr "Mono 1.0"

msgid "Stereo 2.0"
msgstr "Stereo 2.0"

msgid "Headphones 2.0"
msgstr "Cuffie 2.0"

msgid "Stereo 2.1"
msgstr "Stereo 2.1"

msgid "Surround 3.0"
msgstr "Surround 3.0"

msgid "Surround 4.0"
msgstr "Surround 4.0"

msgid "Surround 4.1"
msgstr "Surround 4.1"

msgid "Surround 5.0"
msgstr "Surround 5.0"

msgid "Surround 5.1"
msgstr "Surround 5.1"

msgid "Surround 6.0"
msgstr "Surround 6.0"

msgid "Surround 6.1"
msgstr "Surround 6.1"

msgid "Surround 7.1"
msgstr "Surround 7.1"

msgid "Pass Through"
msgstr "Pass Through"

msgid "very large"
msgstr "molto grande"

msgid "Software"
msgstr "Software"

msgid "Hardware"
msgstr "Hardware"

msgid "no"
msgstr "no"

msgid "grayscale"
msgstr "scala di grigi"

msgid "transparent"
msgstr "trasparente"

msgid "transparent grayscale"
msgstr "scala di grigi trasparente"

msgid "yes"
msgstr "sì"

msgid "nearest"
msgstr "più vicino"

msgid "bilinear"
msgstr "bilineare"

msgid "LUT8"
msgstr ""

msgid "TrueColor"
msgstr ""

msgid "video stream"
msgstr "flusso video"

msgid "none"
msgstr "nessuno"

msgid "nonref"
msgstr "nessun riferimento"

msgid "bidir"
msgstr "bidirezionale"

msgid "nonkey"
msgstr "nessuna chiave"

msgid "all"
msgstr "tutti"

msgid "Frontend initialization failed"
msgstr "Inizializzazione frontend fallita"

msgid "Server initialization failed"
msgstr "Inizializzazione server fallita"

msgid "Playlist"
msgstr "Lista esecuzione"

msgid "Button$Random"
msgstr "Casuale"

msgid "Button$Normal"
msgstr "Normale"

msgid "Button$Add files"
msgstr "Aggiungi files"

msgid "Button$Remove"
msgstr "Rimuovi"

msgid "Button$Mark"
msgstr ""

msgid "Queued to playlist"
msgstr "Accoda alla lista esecuzione"

msgid "Random play"
msgstr "Riproduzione casuale"

msgid "Normal play"
msgstr "Riproduzione normale"

msgid "DVD Menu"
msgstr "Menu DVD"

msgid "Exit DVD menu"
msgstr "Esci dal menu DVD"

msgid "DVD Root menu"
msgstr "Menu principale DVD"

msgid "DVD Title menu"
msgstr "Titolo menu DVD"

msgid "DVD SPU menu"
msgstr "SPU menu DVD"

msgid "DVD Audio menu"
msgstr "Audio menu DVD"

msgid "Close menu"
msgstr "Chiudi menu"

msgid "BluRay Top menu"
msgstr ""

msgid "Toggle Pop-Up menu"
msgstr ""

msgid "Next title"
msgstr ""

msgid "Previous title"
msgstr ""

msgid "Delete image ?"
msgstr "Eliminare immagine ?"

msgid "Play movie title"
msgstr ""

msgid "Play disc"
msgstr ""

msgid "Images"
msgstr "Immagini"

msgid "Play music"
msgstr "Riproduci musica"

msgid "Add to playlist"
msgstr "Aggiungi alla lista esec."

msgid "Play file"
msgstr "Riproduci file"

msgid "Button$Queue"
msgstr "Coda"

msgid "Media"
msgstr "Media"

msgid "View images"
msgstr "Visualizza immagini"

msgid "Play DVD disc"
msgstr "Riproduci disco DVD"

msgid "Play BluRay disc"
msgstr "Riproduci disco BluRay"

msgid "Play audio CD"
msgstr "Riproduci CD audio"

msgid "Video settings"
msgstr "Impostazioni video"

msgid "Play only audio"
msgstr "Riproduci solo audio"

msgid "Crop letterbox 4:3 to 16:9"
msgstr "Ritaglia letterbox 4:3 a 16:9"

msgid "Overscan (crop image borders)"
msgstr "Overscan (ritaglia bordi immagine)"

msgid "Audio settings"
msgstr "Impostazioni audio"

msgid "Audio Compression"
msgstr "Compressione audio"

msgid "Audio equalizer"
msgstr "Equalizzatore audio"

msgid "Local Frontend"
msgstr "Frontend locale"

msgid "Aspect ratio"
msgstr "Formato"

msgid "Video aspect ratio"
msgstr "Formato video"

msgid "On"
msgstr "Attivo"

msgid "Deinterlacing"
msgstr "Deinterlacciamento"

msgid "Upmix stereo to 5.1"
msgstr "Suono da Stereo a 5.1"

msgid "Downmix AC3 to surround"
msgstr "Suono da AC3 a Surround"

msgid "Default playlist not found"
msgstr "Lista esec. predefinita non trovata"

msgid "Default playlist is not symlink"
msgstr "La lista esec. predefinita non è un link simbolico"

msgid "Default playlist not defined"
msgstr "Lista esec. predefinita non definita"

msgid "Delay"
msgstr "Ritardo"

msgid "ms"
msgstr "ms"

#, c-format
msgid "xineliboutput: hotkey %s not binded"
msgstr "xineliboutput: tasto %s non associato"

msgid "Audio"
msgstr "Audio"

msgid "Volume control"
msgstr "Controllo volume"

msgid "Mix to headphones"
msgstr "Suono a cuffie"

msgid "Visualization"
msgstr "Visualizzazione"

msgid "Width"
msgstr "Larghezza"

msgid "px"
msgstr "px"

msgid "Height"
msgstr "Altezza"

msgid "Speed"
msgstr "Velocità"

msgid "fps"
msgstr "fps"

msgid "Background image MRL"
msgstr ""

msgid "Audio Equalizer"
msgstr "Equalizzatore audio"

msgid "Use Video-Out Driver"
msgstr "Utilizza driver uscita video"

msgid "vector"
msgstr "vettoriale"

msgid "full"
msgstr "intero"

msgid "half (top)"
msgstr "metà (superiore)"

msgid "half (bottom)"
msgstr "metà (inferiore)"

msgid "Video"
msgstr "Video"

msgid "Use driver crop"
msgstr "Utilizza driver taglio"

msgid "Autodetect letterbox"
msgstr "Rileva letterbox in automatico"

msgid "Crop to"
msgstr "Ritaglia a"

msgid "Autodetect rate"
msgstr "Rileva frequenza in automatico"

msgid "Stabilize time"
msgstr "Ora stabilizzazione"

msgid "Maximum logo width [%]"
msgstr "Larghezza massima logo [%]"

msgid "Overscan compensate [%1000]"
msgstr "Compensazione Overscan [%1000]"

msgid "Soft start"
msgstr "Avvio leggero"

msgid "Soft start step"
msgstr "Passo avvio lento"

msgid "Detect subtitles"
msgstr "Rileva sottotitoli"

msgid "Subs detect stabilize time"
msgstr "Rileva ora stabilizzazione sottotitoli"

msgid "Subs detect lifetime"
msgstr "Rileva durata sottotitoli"

msgid "Use avards analysis"
msgstr "Utilizza analisi avards"

msgid "Bar tone tolerance"
msgstr "Tolleranza tonalità pan."

msgid "Software scaling"
msgstr "Ridimensionamento software"

msgid "Change aspect ratio"
msgstr "Cambia formato video"

msgid "Change video size"
msgstr "Cambia dimensione video"

msgid "Allow downscaling"
msgstr "Permetti ridimensionamento"

msgid "Post processing (ffmpeg)"
msgstr "Codifica (ffmpeg)"

msgid "Quality"
msgstr "Qualità"

msgid "Mode"
msgstr "Modalità"

msgid "Method"
msgstr "Metodo"

msgid "Cheap mode"
msgstr "Modalità economica"

msgid "Pulldown"
msgstr "Pulldown"

msgid "Frame rate"
msgstr "Frame rate"

msgid "Judder Correction"
msgstr "Correzione gamma"

msgid "Use progressive frame flag"
msgstr "Utilizza flag frame progressivo"

msgid "Chroma Filter"
msgstr "Filtro Chroma"

msgid "Sharpen / Blur"
msgstr "Nitido / Blur"

msgid "Width of the luma matrix"
msgstr "Larghezza della matrice luma"

msgid "Height of the luma matrix"
msgstr "Altezza della matrice luma"

msgid "Amount of luma sharpness/blur"
msgstr "Valore di nitidezza/blur luma"

msgid "Width of the chroma matrix"
msgstr "Larghezza della matrice chroma"

msgid "Height of the chroma matrix"
msgstr "Altezza della matrice chroma"

msgid "Amount of chroma sharpness/blur"
msgstr "Valore di nitidezza/blur chroma"

msgid "3D Denoiser"
msgstr "Denoiser 3D"

msgid "Spatial luma strength"
msgstr "Resistenza luma spaziale"

msgid "Spatial chroma strength"
msgstr "Resistenza chroma spaziale"

msgid "Temporal strength"
msgstr "Resistenza temporale"

msgid "HUE"
msgstr "Tonalità"

msgid "Saturation"
msgstr "Saturazione"

msgid "Contrast"
msgstr "Contrasto"

msgid "Brightness"
msgstr "Luminosità"

msgid "Sharpness"
msgstr "Nitidezza"

msgid "Noise Reduction"
msgstr "Riduzione rumore"

msgid "Smooth fast forward"
msgstr "Avanzamento veloce leggero"

msgid "Fastest trick speed"
msgstr "Trucco velocità più rapida"

msgid "On-Screen Display"
msgstr "Messaggi in sovrimpressione (OSD)"

msgid "Hide main menu"
msgstr "Nascondi voce menu principale"

msgid "Resolution"
msgstr "Risoluzione"

msgid "Color depth"
msgstr ""

msgid "Blending method"
msgstr "Metodo di sfocatura"

msgid "Use hardware for low-res video"
msgstr "Utilizza hardware per video bassa risoluzione"

msgid "Scaling method"
msgstr "Metodo ridimensione"

msgid "Scale subtitles"
msgstr "Scala sottotitoli"

msgid "Show all layers"
msgstr "Mostra tutti i livelli"

msgid "Dynamic transparency correction"
msgstr "Correzione trasparenza dinamica"

msgid "Static transparency correction"
msgstr "Correzione trasparenza statica"

msgid "External subtitle size"
msgstr "Dimensione sottotitoli esterni"

msgid "DVB subtitle decoder"
msgstr "Decoder sottotitoli DVB"

msgid "Decoder"
msgstr "Decoder"

msgid "Buffer size"
msgstr "Dimensione buffer"

msgid "Number of PES packets"
msgstr "Numero di pacchetti PES"

msgid "Local Display Frontend"
msgstr "Frontend visualizzazione locale"

msgid "Use keyboard"
msgstr "Utilizza tastiera"

msgid "Driver"
msgstr "Driver"

msgid "Display address"
msgstr "Mostra indirizzo"

msgid "Framebuffer device"
msgstr "Periferica framebuffer"

msgid "Fullscreen mode"
msgstr "Mod. schermo intero"

msgid "Window width"
msgstr "Larghezza finestra"

msgid "Window height"
msgstr "Altezza finestra"

msgid "Window aspect"
msgstr "Aspetto finestra"

msgid "Scale to window size"
msgstr "Scala a dimensione finestra"

msgid "Port"
msgstr "Porta"

msgid "Speakers"
msgstr "Altoparlanti"

msgid "Remote Clients"
msgstr "Client remoti"

msgid "Allow remote clients"
msgstr "Permetti client remoti"

msgid "Listen port (TCP and broadcast)"
msgstr "Porta in ascolto (TCP e trasmissione)"

msgid "Listen address"
msgstr "Indirizzo in ascolto"

msgid "Remote keyboard"
msgstr "Tastiera remota"

msgid "Max number of clients"
msgstr "Numero massimo di client"

msgid "PIPE transport"
msgstr "Protocollo PIPE"

msgid "TCP transport"
msgstr "Protocollo TCP"

msgid "UDP transport"
msgstr "Protocollo UDP"

msgid "RTP (multicast) transport"
msgstr "Protocollo RTP (multicast)"

msgid "Address"
msgstr "Indirizzo"

msgid "TTL"
msgstr "TTL"

msgid "Transmit always on"
msgstr "Trasmetti sempre"

msgid "SAP announcements"
msgstr "Annunci SAP"

msgid "Server announce broadcasts"
msgstr "Annuncio trasmissioni dal server"

msgid "HTTP transport for media files"
msgstr "Protocollo HTTP per file multimediali"

msgid "Additional network services"
msgstr "Ulteriori servizi di rete"

msgid "HTTP server"
msgstr "Server HTTP"

msgid "HTTP clients can control VDR"
msgstr "I client HTTP possono controllare VDR"

msgid "RTSP server"
msgstr "Server RTSP"

msgid "RTSP clients can control VDR"
msgstr "I client RTSP possono controllare VDR"

msgid "Playlist settings"
msgstr "Impostazioni lista esec."

msgid "Show the track number"
msgstr "Mostra il numero della traccia"

msgid "Show the name of the artist"
msgstr "Mostra il nome dell'artista"

msgid "Show the name of the album"
msgstr "Mostra il nome dell'album"

msgid "Scan for metainfo"
msgstr "Scansione metainfo"

msgid "Cache metainfo"
msgstr "Cache metainfo"

msgid "Arrow keys control DVD playback"
msgstr "Riproduzione DVD con tasti freccia"

msgid "Show hidden files"
msgstr ""

msgid "Allow removing files"
msgstr ""

msgid "Remember last playback position"
msgstr ""

msgid "Media Player"
msgstr "Lettore multimediale"

msgid "Grayscale"
msgstr "Scala di grigi"

msgid "Bitmap"
msgstr "Bitmap"

msgid "OSD"
msgstr "OSD"

msgid "Test Images"
msgstr "Prova immagini"

msgid "X11/xine-lib output plugin"
msgstr "Plugin uscita X11/xine-lib"

#~ msgid "normal"
#~ msgstr "normale"

#~ msgid "inverted"
#~ msgstr "invertito"

#~ msgid "Interlaced Field Order"
#~ msgstr "Ordine campo interlacciato"

#~ msgid "Button$Sort"
#~ msgstr "Ordina"

#~ msgid "Play file >>"
#~ msgstr "Riproduci file >>"

#~ msgid "Play music >>"
#~ msgstr "Riproduci musica >>"

#~ msgid "View images >>"
#~ msgstr "Visualizza immagini >>"

#~ msgid "Play DVD disc >>"
#~ msgstr "Riproduci disco DVD >>"

#~ msgid "Play audio CD >>"
#~ msgstr "Riproduci CD audio >>"

#~ msgid "Play BluRay disc >>"
#~ msgstr "Riproduci disco BluRay >>"

#~ msgid "Play remote DVD >>"
#~ msgstr "Riproduci DVD remoto >>"

#~ msgid "Play remote CD >>"
#~ msgstr "Riproduci CD remoto >>"

#~ msgid "Headphone audio mode"
#~ msgstr "Modalità cuffie audio"

#~ msgid "Audio equalizer >>"
#~ msgstr "Equalizzatore audio >>"
